<?php

use Illuminate\Database\Seeder;

class TodosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('books')->insert([
        [
            'title' => 'Harry Potter',
            'author' =>'JK Rowling',
            'created_at' => date('Y-m-d G:i:s'),
            'user_id' => 1,
       ],
       [
        'title' => 'Harry Potter 2',
        'author' =>'JK Rowling',
        'created_at' => date('Y-m-d G:i:s'),
        'user_id' => 1,
    ],
   [
    'title' => 'Harry Potter 3',
    'author' =>'JK Rowling',
    'created_at' => date('Y-m-d G:i:s'),
    'user_id' => 0,
],
[
    'title' => 'Harry Potter 4',
    'author' =>'JK Rowling',
    'created_at' => date('Y-m-d G:i:s'),
    'user_id' => 1,
],
[
    'title' => 'Harry Potter 5',
    'author' =>'JK Rowling',
    'created_at' => date('Y-m-d G:i:s'),
    'user_id' => 0,
],

                ]);
    }
}