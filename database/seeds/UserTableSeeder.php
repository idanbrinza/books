<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('users')->insert([
           [
                'name' => 'Jack',
                'email' =>'jack@jack.zibi',
                'password' =>'zibzibi',
                'created_at' => date('Y-m-d G:i:s'),
           ],

           [
                'name' => 'John',
                'email' =>'John@John.zibi',
                'password' =>'zibzibi',
                'created_at' => date('Y-m-d G:i:s'),
           ],
                ]);
    }
}